﻿/* PRACTICA NUMERO: 3*/

USE m1u2practica3;

/* CONSULTA 1 */
-- Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento.

  SELECT 
    e.dept_no, COUNT(*) numero 
  FROM emple e 
  GROUP BY e.dept_no;


/* CONSULTA 2 */
-- Visualizar los departamentos con más de 5 empleados. Utilizar GROUP BY para agrupar por departamento y HAVING para
-- establecer la condición sobre los grupos.

   SELECT 
    e.dept_no 
   FROM emple e 
   GROUP BY e.dept_no 
   HAVING COUNT(*)>5;

/* CONSULTA 3 */
-- Hallar la media de los salarios de cada departamento (utilizar la función avg y GROUP BY).

   SELECT 
    e.dept_no, AVG(e.salario) media 
   FROM emple e
   GROUP BY e.dept_no; 
  
   
/* CONSULTA 4 */
-- Visualizar el nombre de los empleados vendedores del departamento ʻVENTASʼ (Nombre del departamento=ʼVENTASʼ,
-- oficio=ʼVENDEDORʼ).

    
   SELECT 
    e.apellido 
   FROM emple e 
   JOIN depart d 
   ON e.dept_no = d.dept_no 
   WHERE e.oficio='VENDEDOR' AND d.dnombre='VENTAS';


/* CONSULTA 5 */
-- Visualizar el número de vendedores del departamento ʻVENTASʼ (utilizar la función COUNT sobre la consulta anterior).

   SELECT 
      COUNT(*) numero 
   FROM
    (
     SELECT e.apellido FROM emple e JOIN depart d ON e.dept_no = d.dept_no WHERE e.oficio='VENDEDOR' AND d.dnombre='VENTAS'
     ) c1; 


/* CONSULTA 6 */
-- Visualizar los oficios de los empleados del departamento ʻVENTASʼ.
  SELECT 
    DISTINCT e.oficio
  FROM emple e 
  JOIN depart d 
  ON e.dept_no = d.dept_no 
  WHERE d.dnombre='VENTAS';


/* CONSULTA 7 */
-- A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea ʻEMPLEADOʼ (utilizar
-- GROUP BY para agrupar por departamento. En la cláusula WHERE habrá que indicar que el oficio es ʻEMPLEADOʼ).

  SELECT 
    e.dept_no, COUNT(*) nEmpleados 
  FROM emple e 
  WHERE e.oficio='EMPLEADO' 
  GROUP BY e.dept_no;


/* CONSULTA 8 */
-- Visualizar el departamento con más empleados.

  -- c1: cuento los empleados por departamento

  SELECT 
    e.dept_no, COUNT(*) nEmpleados
  FROM emple e 
  GROUP BY e.dept_no;

  -- c2: calculo el máximo del numero de empleados
  SELECT MAX(c1.nEmpleados) FROM 
    (
    SELECT 
      e.dept_no, COUNT(*) nEmpleados
    FROM emple e 
    GROUP BY e.dept_no
    ) c1;

  -- final: con un JOIN
    SELECT c1.dept_no FROM 
      (
       SELECT 
          e.dept_no, COUNT(*) nEmpleados
       FROM emple e 
       GROUP BY e.dept_no
      ) c1
      JOIN 
      (
       SELECT MAX(c1.nEmpleados) maximo FROM 
        (
         SELECT 
            e.dept_no, COUNT(*) nEmpleados
         FROM emple e 
         GROUP BY e.dept_no
        ) c1
      ) c2
      ON c1.nEmpleados=c2.maximo;


/* CONSULTA 9 */
-- Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados.

  -- c1: calculo la media de los salarios

  SELECT AVG(e.salario) mediaSalario FROM emple e;

  -- c2: calculo la suma de los salarios por departamento

  SELECT e.dept_no, SUM(e.salario) sumaSalarios FROM emple e GROUP BY e.dept_no;

  -- final:
  SELECT e.dept_no FROM emple e
    GROUP BY e.dept_no
    HAVING SUM(e.salario)>
    (
      SELECT AVG(e.salario) mediaSalario FROM emple e
    );


/* CONSULTA 10 */
-- Para cada oficio obtener la suma de salarios.

  SELECT
    e.oficio, SUM(e.salario) sumaSalarios 
  FROM emple e 
  GROUP BY e.oficio;


/* CONSULTA 11 */
-- Visualizar la suma de salarios de cada oficio del departamento ʻVENTASʼ.

    -- c1: calculo el dept_no del departamento VENTAS
    SELECT d.dept_no FROM depart d WHERE d.dnombre='VENTAS';

    -- final
    SELECT e.oficio, SUM(e.salario) sumaSalarios
      FROM (
      SELECT d.dept_no FROM depart d WHERE d.dnombre='VENTAS'
        ) c1
      JOIN 
      emple e 
      ON c1.dept_no=e.dept_no
    GROUP BY e.oficio;


    -- opcion más directa:

    SELECT 
      oficio, SUM(salario) sumaSalarios 
    FROM emple 
    JOIN depart d
    ON emple.dept_no = d.dept_no 
    WHERE d.dnombre='VENTAS' 
    GROUP BY oficio;

/* CONSULTA 12 */
-- Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado.
  
  -- c1: numero de empleados con oficio empleado por departamento
  SELECT 
    e.dept_no, COUNT(*) nEmpleados
  FROM emple e 
  WHERE e.oficio='EMPLEADO' 
  GROUP BY e.dept_no;

  -- c2: calculo máximo del numero de empleados
  SELECT MAX(c1.nEmpleados) maximo FROM 
    (
     SELECT 
      e.dept_no, COUNT(*) nEmpleados
     FROM emple e 
     WHERE e.oficio='EMPLEADO' 
     GROUP BY e.dept_no
    ) c1;

  -- final:
  SELECT c1.dept_no FROM (
      SELECT 
        e.dept_no, COUNT(*) nEmpleados
      FROM emple e 
      WHERE e.oficio='EMPLEADO' 
      GROUP BY e.dept_no
      ) c1
      JOIN 
      (
        SELECT MAX(c1.nEmpleados) maximo FROM 
        (
         SELECT 
          e.dept_no, COUNT(*) nEmpleados
         FROM emple e 
         WHERE e.oficio='EMPLEADO' 
         GROUP BY e.dept_no
        ) c1
      ) c2
      ON c1.nEmpleados=c2.maximo;


/* CONSULTA 13 */
-- Mostrar el número de oficios distintos de cada departamento.

   SELECT 
    e.dept_no, COUNT(DISTINCT e.oficio) 
   FROM emple e
   GROUP BY e.dept_no;

  
/* CONSULTA 14 */
-- Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión.

  SELECT 
    DISTINCT dept_no
  FROM emple e 
  GROUP BY dept_no,e.oficio
  HAVING COUNT(*)>2;

 
  
/* CONSULTA 15 */
-- Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades.
  SELECT 
    h.estanteria, SUM(h.unidades) sumaUnidades 
  FROM herramientas h 
  GROUP BY h.estanteria;

/* CONSULTA 16 */
-- Visualizar la estantería con más unidades de la tabla HERRAMIENTAS. (con totales y sin totales)

  -- c1: consulta 15, suma de las unidades por estanteria
  SELECT 
    h.estanteria, SUM(h.unidades) sumaUnidades 
  FROM herramientas h 
  GROUP BY h.estanteria;

  -- c2: calculo máximo de la suma
  SELECT MAX(c1.sumaUnidades) FROM (
    SELECT 
      h.estanteria, SUM(h.unidades) sumaUnidades 
    FROM herramientas h 
    GROUP BY h.estanteria
    ) c1;

  -- final
  SELECT c1.estanteria FROM 
    (
    SELECT 
      h.estanteria, SUM(h.unidades) sumaUnidades 
    FROM herramientas h 
    GROUP BY h.estanteria      
    ) c1
    JOIN 
    (
      SELECT MAX(c1.sumaUnidades) maximo FROM (
        SELECT 
           h.estanteria, SUM(h.unidades) sumaUnidades 
        FROM herramientas h 
        GROUP BY h.estanteria
        ) c1
    ) c2
    ON c1.sumaUnidades=c2.maximo;

/* CONSULTA 17 */
-- Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital.

  SELECT 
    m.cod_hospital, COUNT(*) nMedicos 
  FROM medicos m 
  GROUP BY m.cod_hospital 
  ORDER BY m.cod_hospital DESC;
  

/* CONSULTA 18 */
-- Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene.

  SELECT
    DISTINCT m.cod_hospital,  m.especialidad 
  FROM medicos m 
  ORDER BY m.cod_hospital;
 

/* CONSULTA 19 */
  -- Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos (tendrás que partir
  -- de la consulta anterior y utilizar GROUP BY).

  SELECT 
    c1.cod_hospital, c1.especialidad, COUNT(*) nMedicos 
  FROM 
    (
     SELECT
        m.cod_hospital,  m.especialidad 
     FROM medicos m 
    ) c1
    GROUP BY c1.cod_hospital, c1.especialidad;
 

/* CONSULTA 20 */
  -- Obtener por cada hospital el número de empleados.

  SELECT 
    p.cod_hospital,h.nombre, COUNT(*) nEmpleados
  FROM personas p 
  JOIN hospitales h
  USING (cod_hospital)
  GROUP BY p.cod_hospital;

   

  /* CONSULTA 21 */
  -- Obtener por cada especialidad el número de trabajadores.
  SELECT 
    m.especialidad,COUNT(*) 
  FROM medicos m 
  GROUP BY m.especialidad;


  /* CONSULTA 22 */
  -- Visualizar la especialidad que tenga más médicos
    -- c1: calcular máximo
  SELECT 
    MAX(numero)
  FROM (
    SELECT m.especialidad,COUNT(*) numero FROM medicos m GROUP BY m.especialidad
  ) c1;

  -- final
  SELECT 
    m.especialidad 
  FROM medicos m 
  GROUP BY m.especialidad
  HAVING COUNT(*)=
    (SELECT 
      MAX(numero)
    FROM (
            SELECT m.especialidad,COUNT(*) numero FROM medicos m GROUP BY m.especialidad
         )c1
  );

   
  /* CONSULTA 23 */
  -- ¿Cuál es el nombre del hospital que tiene mayor número de plazas?

    -- c1: calculo el número máximo de plazas que hay
    SELECT MAX(h.num_plazas) maximo FROM hospitales h;

    -- final
    SELECT h.nombre 
    FROM hospitales h 
    WHERE 
      h.num_plazas=(SELECT MAX(h.num_plazas) maximo FROM hospitales h);

   
  /* CONSULTA 24 */
  -- Visualizar las diferentes estanterías de la tabla HERRAMIENTAS ordenados descendentemente por estantería.

    SELECT 
      DISTINCT h.estanteria
    FROM herramientas h 
    ORDER BY h.estanteria DESC;
 

  /* CONSULTA 25 */
  -- Averiguar cuántas unidades tiene cada estantería.

    SELECT 
      h.estanteria, SUM(h.unidades) nUnidades
    FROM herramientas h 
    GROUP BY h.estanteria;

  /* CONSULTA 26 */
  -- Visualizar las estanterías que tengan más de 15 unidades

    SELECT h.estanteria, 
      SUM(h.unidades) nUnidades 
    FROM herramientas h 
    GROUP BY h.estanteria 
    HAVING nUnidades>15;


  /* CONSULTA 27 */
  -- ¿Cuál es la estantería que tiene más unidades?

    -- c1: cuento las unidades que hay en cada estantería
  SELECT h.estanteria, SUM(h.unidades) nUnidades FROM herramientas h GROUP BY h.estanteria;

    -- c2: calculo el máximo del número de unidades
  SELECT MAX(c1.nUnidades) FROM (SELECT h.estanteria, SUM(h.unidades) nUnidades FROM herramientas h GROUP BY h.estanteria) c1;

    -- final
  SELECT c1.estanteria FROM (
    SELECT h.estanteria, SUM(h.unidades) nUnidades FROM herramientas h GROUP BY h.estanteria) c1
    JOIN (SELECT MAX(c1.nUnidades) maximo FROM (SELECT h.estanteria, SUM(h.unidades) nUnidades FROM herramientas h GROUP BY h.estanteria) c1
    ) c2
    ON c1.nUnidades=c2.maximo;

  
  /* CONSULTA 28 */
  -- A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado.

   
    -- c1: departamentos en la tabla empleados
    SELECT
      DISTINCT e.dept_no 
    FROM emple e;

    -- final
    SELECT 
      * 
    FROM depart d 
    LEFT JOIN (SELECT DISTINCT e.dept_no FROM emple e) c1 
    USING (dept_no) 
    WHERE c1.dept_no IS NULL;


  /* CONSULTA 29 */
  -- Mostrar el número de empleados de cada departamento. En la salida se debe mostrar también los departamentos que no
  -- tienen ningún empleado.

    -- c1: cuento los empleados por departamento
    SELECT e.dept_no, COUNT(*) numero FROM emple e GROUP BY e.dept_no;

    -- final: hago un left join de todos los departamentos con la consulta c1
    SELECT 
      d.dnombre, numero 
    FROM depart d 
    LEFT JOIN (SELECT dept_no, COUNT(*) numero FROM emple e GROUP BY e.dept_no) c1
    USING (dept_no);


  /* CONSULTA 30 */
  -- Obtener la suma de salarios de cada departamento, mostrando las columnas DEPT_NO, SUMA DE SALARIOS y DNOMBRE.
  -- En el resultado también se deben mostrar los departamentos que no tienen asignados empleados.

    SELECT
      d.dept_no, SUM(e.salario) 'SUMA DE SALARIOS', d.dnombre
    FROM depart d 
    LEFT JOIN emple e 
    USING(dept_no) 
    GROUP BY d.dept_no;


  /* CONSULTA 31 */
  -- Utilizar la función IFNULL en la consulta anterior para que en el caso de que un departamento no tenga empleados, aparezca
  -- como suma de salarios el valor 0.

  SELECT
      d.dept_no, SUM(IFNULL(e.salario,0)) 'SUMA DE SALARIOS', d.dnombre
    FROM depart d 
    LEFT JOIN emple e 
    USING(dept_no) 
    GROUP BY d.dept_no;

 
  /* CONSULTA 32 */
  -- Obtener el número de médicos que pertenecen a cada hospital, mostrando las columnas COD_HOSPITAL, NOMBRE y
  -- NÚMERO DE MÉDICOS. En el resultado deben aparecer también los datos de los hospitales que no tienen médicos.
  
    SELECT 
      h.cod_hospital, h.nombre, COUNT(m.cod_hospital) 'NUMERO DE MEDICOS'
    FROM medicos m 
    RIGHT JOIN hospitales h 
    ON m.cod_hospital = h.cod_hospital 
    GROUP BY m.cod_hospital;

    

    
